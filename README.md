# Cybertron

This is a Slackbot documenting whether teams have succesfully crossed the line in their latest matches in "Sandstorm"
and teleoperated periods. This has allowed [team 2468](http://www.frc2468.org/) to find and help other teams at
competition which need programming help throughout the 2019 FRC game, Deep Space.

If a team:

- Crosses in Sandstorm: They will not show on the list. They're likely okay.
- Crosses in Teleop: They may not have a working Sandstorm mode or may need help.
- Doesn't cross: Their robot may not work overall or they may need help.

Available commands:

- `!rollout`: Print the list of teams which may need help and their status.
- `!setcomp [competition name]`: Choose data to populate list from.
- `!helped`: Print the list of teams to ignore until their robot fails again.
  - `!helped [team #]` Add or remove a team from the list.
  - `!helped clear` Erase the list.
- `!blacklist`: Print the list of teams to ignore altogether.
  - `!blacklist [team #]` Add or remove a team from the list.
  - `!blacklist clear` Erase the list.

Bot configuration: Write the following in `src/config.json`, filling in the specifics and private API keys. Do not share these
or your config file with strangers; that's just asking to be hacked!

```js
{
  "blueAllianceKey": "Blue Alliance token goes here",
  "competitionName": "world champs", // Current competition's name.
  "botToken": "Slackbot user token goes here",
  "channelID": "GH724E1TN", // ID to Slack channel where Cybertron works.
  "victoryEmoji": ":dino_nugget3:", // Slack emoji to display when all teams are helped!
  "maxLinesPerMessage": 100, // What length at which to split messages (can be left out.)
  "competitions": {
    "greenville": "2019txgre", // Name and Blue Alliance key.
    "galileo": "2019gal",
    "world champs": [ // Multiple Blue Alliance keys to scan multiple competitions.
      "2019carv",
      "2019gal",
      "2019hop",
      "2019new",
      "2019roe",
      "2019tur"
    ]
  }
}
```

**NOTE**: Before marking a team as helped, run `!rollout` after their latest match results are in, and on Blue Alliance.
Otherwise, the bot will not know after what match the team was reported helped or when to re-add them to the `!rollout` list.