// Imports.
import WebHooks from "node-webhooks";
import fetch from "node-fetch";
import {CLIENT_EVENTS, RTM_EVENTS, RtmClient} from "@slack/client";

// Runtime variables.
let {
    blueAllianceKey,
    competitionName,
    botToken,
    channelID,
    competitions,
    maxLinesPerMessage,
    victoryEmoji
} = process.env.TESTING !== undefined ?
    require(`./config/config.${process.env.TESTING}`) :
    require("./config/config.frc");

if (typeof maxLinesPerMessage !== "number")
    maxLinesPerMessage = 15;
let competitionKeySet = competitions[competitionName];

// Blue Alliance and Slack API.
let blueAlliance = "https://www.thebluealliance.com/api/v3";
let headers = {
    "Content-Type": "application/json",
    "X-TBA-Auth-Key": blueAllianceKey
};

let rtm = new RtmClient(botToken);

// Type definitions and client variables.
type Channel = { is_member: boolean, is_general: boolean, id: string, name: string };

let channels: Channel[] = [];
let code_appreciators: Channel | undefined = undefined;
let username: string | undefined = undefined;
let purpleList: Set<number> = new Set();

let teamStates: {
    [team: string]: {
        nextMatch: string,
        handled: boolean
    }
} = {};

// Generate score report from Blue Alliance data.
async function checkSandstorms(competitionKeySet: string | string[]): Promise<string[]> {
    if (!Array.isArray(competitionKeySet)) {
        competitionKeySet = [competitionKeySet];
    }

    let output = "";

    try {
        for (let competitionKey of competitionKeySet) {
            if (competitionKeySet.length > 1)
                output += `In ${competitionKey}:` + "\n";

            // Check the API is working.
            let statusResponse = await fetch(`${blueAlliance}/status`, {headers});
            let status = await statusResponse.json();

            // Get a list of teams at the competition, and sort them by number.
            let competitionResponse = await fetch(`${blueAlliance}/event/${competitionKey}/teams/statuses`, {headers});
            let competitionTeams = await competitionResponse.json();

            let teamNumbers = Object.keys(competitionTeams).sort((team1, team2) => {
                let number1 = parseInt(team1.substring(3));
                let number2 = parseInt(team2.substring(3));

                return number1 - number2
            });

            // Stop if any teams are associated with null objects.
            if (!teamNumbers.every(team => competitionTeams[team] !== null)) {
                output += "One or more teams have no information." + "\n";
                output += "Has the competition started?" + "\n";

                return;
            }

            for (let team of teamNumbers) {
                // Get their last match.
                let lastMatch = competitionTeams[team].last_match_key;
                let nextMatch = competitionTeams[team].next_match_key;

                let teamNumber = parseInt(team.substring(3));

                // If a match passes and a team still fails, re-add them.
                if (teamStates[teamNumber] === undefined || teamStates[teamNumber].nextMatch !== nextMatch) {
                    teamStates[teamNumber] = {
                        nextMatch: nextMatch as string,
                        handled: false
                    };
                }

                // If they are still handled or blacklisted, ignore the team.
                if (teamStates[teamNumber].handled || purpleList.has(teamNumber)) {
                    continue;
                }

                // Check they have had a match.
                if (lastMatch !== null) {
                    // Get match information.
                    let matchResponse = await fetch(`${blueAlliance}/match/${lastMatch}`, {headers});
                    let match = await matchResponse.json();
                    let matchNumber = match.match_number;

                    let competitionStage;
                    switch (match.comp_level) {
                        case 'qm':
                            competitionStage = "qualification";
                            break;
                        case 'qf':
                            competitionStage = "quarterfinal";
                            break;
                        case 'sf':
                            competitionStage = "semifinal";
                            break;
                        case 'f':
                            competitionStage = "final";
                            break;
                    }

                    if (match.alliances === null) {
                        output += `Latest match results for ${team} not in.` + "\n";
                        continue;
                    }

                    // Find robot in match layout.
                    let alliance = match.alliances.blue.team_keys.includes(team) ? "blue" : "red";

                    let robotPosition = match.alliances[alliance].team_keys.indexOf(team) + 1;
                    let habLineStatus = match.score_breakdown[alliance][`habLineRobot${robotPosition}`];

                    if (habLineStatus !== "CrossedHabLineInSandstorm") {
                        output += `${team} in ${competitionStage} match ${matchNumber}: ${habLineStatus} only!` + "\n"
                    }
                } else {
                    output += `${team} has not had any matches yet.` + "\n"
                }
            }
        }
    } catch (exception) {
        output += `Encountered a ${exception.name}:` + "\n";
        output += exception.message + "\n";
    }

    if (output === "") {
        return [`No teams left! ${victoryEmoji}` + "\n"];
    } else {
        // Output the team information in code blocks.
        output = output.trim();

        // Split the output into maxLinesPerMessage line blocks.
        let messageArray: string[] = [];

        // Build messages until no output is left.
        while (output !== "") {
            let message = "";

            // Put maxLinesPerMessage lines in this message.
            for (let i = 0; i < maxLinesPerMessage && output !== ""; i++) {
                let line;

                // Swallow next line and remove from output string.
                if (output.includes("\n")) {
                    line = output.substring(0, output.indexOf("\n"));
                    output = output.substring(output.indexOf("\n") + 1);
                } else {
                    line = output;
                    output = "";
                }

                message += line + "\n";
            }

            // Add the code-blocked message to the array.
            messageArray.push("```" + message.trim() + "```");
        }

        // Return list of messages to send.
        return messageArray;
    }
}

// When the client receives a message.
rtm.on(RTM_EVENTS.MESSAGE, async function (message: { text: string, channel: string }) {
    let text = message.text;
    if (!text) return;

    console.log(`Got message ${text} in ${message.channel}`);

    // Regular expressions for recognizing commands.
    let rollOutRegex = /!rollout/;
    let changeCompRegex = /!(set|change)comp\s(.+)/;
    let howAreYouRegex = /how\sare\syou|what'?s\sup/;
    let purpleListRegex = /!(purple|black|white)list(\s(.+))?/;
    let handledRegex = /!(confirm|confirmed|handled|affirm|verify|help|check)(\s(.+))?/;
    let deleteRegex = /clear|erase|reset/;
    let nameRegex = /cybertron/;

    if (message.channel == channelID) {
        let command = text.toLowerCase();

        if (rollOutRegex.test(command)) {
            let sandstorms = await checkSandstorms(competitionKeySet);

            for (let response of sandstorms) {
                await rtm.sendMessage(response, channelID);
            }
        } else if (changeCompRegex.test(command)) {
            // SET COMPETITION
            let matches = changeCompRegex.exec(command);
            let competition = matches[2];

            // Check whether we have a key for the specified competition.
            if (competitions[competition] !== undefined) {
                let oldCompetition = competitionKeySet;
                competitionKeySet = competitions[competition];

                await rtm.sendMessage(`Changing to ${competitionKeySet} from ${oldCompetition}`, message.channel);
            } else {
                await rtm.sendMessage(`Could not find competition ${competition}`, message.channel);
            }
        } else if (purpleListRegex.test(command)) {
            // PERMANENT BLACKLIST
            let matches = purpleListRegex.exec(command);

            // If no team is specified, print out the list.
            if (matches[3] === undefined) {
                let output = "";

                await purpleList.forEach(team => {
                    output += team + "\n";
                });

                if (output !== "")
                    output = "Current list:\n```" + output + "```";
                else
                    output = "The list is empty.";

                await rtm.sendMessage(output, channelID);
            } else if (deleteRegex.test(matches[3].toLowerCase())) {
                purpleList = new Set();

                await rtm.sendMessage("The list has been emptied.", message.channel);
            } else {
                let team = parseInt(matches[3]);

                // Check whether the team is already blacklisted.
                if (!purpleList.has(team)) {
                    purpleList.add(team);

                    await rtm.sendMessage(`Adding team ${team} to the help blacklist.`, message.channel);
                } else {
                    purpleList.delete(team);

                    await rtm.sendMessage(`Removing team ${team} from the help blacklist.`, message.channel);
                }
            }
        } else if (handledRegex.test(command)) {
            // TEMPORARY CHECKLIST
            let matches = handledRegex.exec(command);

            // If no team is specified, print out the list.
            if (matches[2] === undefined) {
                let output = "";

                for (let team of Object.keys(teamStates)) {
                    if (teamStates[team].handled)
                        output += `${team} next up in ${teamStates[team].nextMatch}` + "\n";
                }

                if (output !== "")
                    output = "Current list:\n```" + output + "```";
                else
                    output = "The list is empty.";

                await rtm.sendMessage(output, channelID);
            } else if (deleteRegex.test(matches[2].toLowerCase())) {
                teamStates = {};

                await rtm.sendMessage("The list has been emptied.", message.channel);
            } else {
                let team = parseInt(matches[3]);

                // Check whether the team has had a match.
                if (teamStates[team] === undefined) {
                    await rtm.sendMessage(`No matches recorded for team ${team}.`, message.channel);
                } else {
                    teamStates[team].handled = !teamStates[team].handled;

                    if (teamStates[team].handled) {
                        await rtm.sendMessage(`Recording team ${team} as helped.`, message.channel);
                    } else {
                        await rtm.sendMessage(`Recording team ${team} as not helped.`, message.channel);
                    }
                }
            }
        } else if (howAreYouRegex.test(command) && nameRegex.test(command)) {
            // HOW ARE YOU, CYBERTRON?
            await rtm.sendMessage("I'm swell, how are you?", channelID);
        }
    }
});

// When the client connects online.
rtm.on(CLIENT_EVENTS.RTM.AUTHENTICATED, (rtmStartData) => {
    channels = rtmStartData.channels.filter(c => c.is_member);
    console.log(`Logged in as ${rtmStartData.self.name} of team ${rtmStartData.team.name}, but not yet connected to a channel`);

    username = `<@${rtmStartData.self.id}>`;
});

// When the client connects to Slack channels.
rtm.on(CLIENT_EVENTS.RTM.RTM_CONNECTION_OPENED, async function () {
    for (const channel of channels) {
        if (channel.is_member && channel.name == "code_appreciators") {
            channelID = channel.id;
            code_appreciators = channel;
        }
    }

    console.log(`Autobots channel: ${code_appreciators}`);
});

rtm.start();
